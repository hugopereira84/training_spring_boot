package com.softwaymedical.back.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/")
public class InformationController {

    @Autowired
    private Environment env;

    @RequestMapping(value = "information/{name}", method = RequestMethod.GET)
    public String hello(@PathVariable(value ="name") String name){
        return "Hello "+name + ". App name: "+env.getProperty("dev.application.name")+". Pom artfact id:_"+env.getProperty("info.app.name");
    }
}
