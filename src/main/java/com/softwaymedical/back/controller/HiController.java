package com.softwaymedical.back.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HiController {

    @RequestMapping("/hi")
    public String hello(Model model){
        model.addAttribute("message", "Hi from Spring MVC");
        return "hi";
    }
}
