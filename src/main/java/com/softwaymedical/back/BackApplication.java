package com.softwaymedical.back;

import com.softwaymedical.back.model.Product;
import com.softwaymedical.back.repository.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@SpringBootApplication
public class BackApplication implements CommandLineRunner {
	private ProductRepository productRepository;
	private Logger logger = LoggerFactory.getLogger(BackApplication.class);

	@Autowired
	public void productRepository(ProductRepository productRepository) {
		/** Best way to do auto wired, because your using dependency injection **/
		this.productRepository = productRepository;
	}

	public static void main(String[] args) {
		SpringApplication.run(BackApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		/* Always going to be run when the app starts */
		Product product1 = new Product();
		product1.setName("Test Product");
		product1.setDescription("Test Product description");
		product1.setCategory("Test");
		product1.setType("GENERAL");
		product1.setPrice(0.0);

		productRepository.save(product1);

		Product product2 = new Product();
		product2.setName("Test Product 2");
		product2.setDescription("Test Product description");
		product2.setCategory("Test");
		product2.setType("CUSTOM");
		product2.setPrice(1.0);

		Product product3 = new Product();
		product3.setName("Test Product 3");
		product3.setDescription("Test Product description 3");
		product3.setCategory("Test 3");
		product3.setType("CUSTOM");
		product3.setPrice(2.0);

		Product product4 = new Product();
		product4.setName("Test Product 4");
		product4.setDescription("Test Product description 4");
		product4.setCategory("Test 4");
		product4.setType("SPECIFIC");
		product4.setPrice(2.0);

		List <Product> listProducts = new ArrayList<>(Arrays.asList(product2, product3, product4));

		productRepository.saveAll(listProducts);

		/* @info: this is to test streams / lambda */
		/*String testString = "This is a test string";
		List<Product> products = productRepository.findAll();
		products.stream().map(p -> "Product:"+p.getName()+", Product Description:"+p.getDescription());
		String ListProducts = products
				.stream()
				.map(p -> {
					System.out.println("Value:"+testString);
					logger.info("Entering mapping. Product name:"+p.toString() );
					return "Product:"+p.getName()+", Product Description:"+p.getDescription();
				})
				.collect(Collectors.joining());*/

		//Product resultProduct = productRepository.findByType("GENERAL");
		//logger.info("General product found: "+resultProduct.toString());

		/*Collection resultsType = productRepository.findAllByType("CUSTOM");
		logger.info("Collection of products by type found: "+resultsType.toString());

		Collection resultsDescriptionCategory = productRepository.findByDescriptionAndCategory("Test Product description", "Test");
		logger.info("List of products by description / category found: "+resultsDescriptionCategory.toString());

		List resultsCategoryNameIn = productRepository.findByCategoryAndNameIn("Test", Arrays.asList("Test Product", "Test Product 2"));
		logger.info("List of products by description / category found: "+resultsCategoryNameIn.toString());*/

		Product productToUpdate = productRepository.findByType("SPECIFIC");
		if(null != productToUpdate){
			logger.info("Before Updated product details:" + productToUpdate.toString());
			productToUpdate.setName("Update test product 4");
			productToUpdate.setDescription("Updated description");

			Product updated = productRepository.save(productToUpdate);
			logger.info("Updated product details:" + updated.toString());
		}

		// Delete products
		//productRepository.delete(product4);

		Collection foundProduct = productRepository.findAllByType("CUSTOM");
		if(null != foundProduct){
			logger.info("Product count in database:"+productRepository.count());
			productRepository.deleteAll(foundProduct);
			logger.info("Product is deleted");
			logger.info("Product count in database:"+productRepository.count());
		}

		Product foundProductSpe = productRepository.findByType("SPECIFIC");
		if(null != foundProductSpe){
			logger.info("Product count in database:"+productRepository.count());
			productRepository.delete(foundProductSpe);
			logger.info("Product is deleted");
			logger.info("Product count in database:"+productRepository.count());
		}
	}
}
