FROM openjdk:15-slim
ADD target/compliance-parent-0.0.1-SNAPSHOT.jar compliance.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "compliance.jar"]